WORKDIR=$(pwd)

cd libfindrtp-0.4b

make

make install

cd $WORKDIR/hack_library

make

cd $WORKDIR/g711conversions

make

cd $WORKDIR

make

strip rtpinsertsound

cp -Rf rtpinsertsound /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX/bin
chmod -R 755 /opt/ANDRAX/bin
